## CERN-HSF GSoC 2019
### _Testing framework for Jupyter notebooks_
-----


This repository contains the exercise to evaluate students interested in applying for the project _"Testing framework for Jupyter notebooks"_, which is part of the [Google Summer of Code (GSoC)](https://summerofcode.withgoogle.com/) program 2019 and is proposed by the [Information Technology Department](http://information-technology.web.cern.ch/) and the [Experimental Physics Department](https://ep-dep.web.cern.ch/) at [CERN](https://home.cern)

The detailed description of the project can be found at https://hepsoftwarefoundation.org/gsoc/2019/proposal_JupyterTest.html

Please follow the guidelines below to go through the exercise. We recommend you start with the first task and, only if you successfully complete it, you proceed to the following ones. Once you complete any of the tasks of the exercise, send back to the mentors the requested deliverables via email. Also, do not hesitate to contact the mentors if you have questions or need clarifications on the exercise.


#### Exercise for Candidate Students
-----
In this exercise you need to work with and combine different technologies together: [Python](https://www.python.org/), [Jupyter Notebooks](https://jupyter.org/), [Bash](https://www.gnu.org/software/bash/), and [Docker](https://www.docker.com/).

In preparation for the exercise, you need to install Jupyter on your machine and Docker. Installation instructions are available online.


##### _Task 1_
Write a simple Python program called help\_robinson.py which:
* Downloads the text of "The Life and Adventures of Robinson Crusoe" by Daniel Defoe ([fulltext](http://www.gutenberg.org/files/521/521-0.txt))
* Counts the words "island" and "islands" (case-insensitive)
* Writes the results into a simple text file islands.txt in the format: `<word> <count>`

_Deliverables:_
1. The Python program help\_robinson.py
2. The output file with the count results islands.txt


##### _Task 2_
By leveraging on the notebook server installed on your local machine during the preparation step, start a session and produce a simple Python notebook called storage\_test.ipynb which:
* Writes files to the local disk and fills them with random bytes. The number of files to be written and their size must be configurable.
* Reads the files back and verifies their consistency, i.e., the files are not corrupted on disk
* Outputs the statistics for each file in a line format: `<name> <size> <corruption_boolean>`

_Deliverables:_
1. The Jupyter notebook storage\_test.ipynb
2. The output of a sample run with 10 files of 1MB each


##### _Task 3_
Write a Dockerfile called storage\_test.Dockerfile which:
* Copies the notebook storage\_test.ipynb produced in Task 2 in the Docker container
* Converts the storage\_test.ipynb notebook to a plain Python script called storage\_test.py. Note: The Jupyter [nbconvert](https://github.com/jupyter/nbconvert) tool can be helpful for this task
* Executes the Python script storage\_test.py when the container starts

Once done, build the Docker image, tag it as storage-test:latest and run it.
Contrast the output obtained from the notebook storage\_test.ipynb against the one of the Docker image.

_Deliverables:_
1. The Dockerfile storage\_test.Dockerfile
2. The commands to build, tag, and build the Docker image
3. The output obtained from the execution of the Docker image


##### _Task 4_
Write a Python script called controller.py which:
* Uses the Python library for the Docker Engine API to interact with the Docker daemon
* Runs the Docker container storage-test:latest built at Task 3
* Parses the output of the container and infers whether one or more files are reported as corrupted

_Deliverables:_
1. The Python script controller.py

##### _Task 5 -- Bonus --_
Repeat Task 4 writing a Bash script called controller.sh that replaces the previous Python script controller.py


#### Deliverables
-----
Once you complete any of the tasks of this exercise, please send us - by e-mail - the requested deliverable to:
enrico.bocchi@cern.ch, 
joao.calado.vicente@cern.ch, 
diogo.castro@cern.ch, 
jakub.moscicki@cern.ch, and 
enric.tejedor.saavedra@cern.ch
